import React from 'react';
import Nav from './Nav';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeeList from './AttendeeList';
import AttendeeForm from './AttendeeForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import { BrowserRouter, Routes, Route } from 'react-router-dom'

function App(props) {
  if (props.attendees === undefined){
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      {/* <div className="container"> */}
        <Routes>
          <Route path="locations">
            <Route path='new' element={<LocationForm />}/>
          </Route>
          <Route path="conferences">
            <Route path='new' element={<ConferenceForm />}/>
          </Route>
          <Route path="attendees">
            <Route path='' element={<AttendeeList attendees={props.attendees} />} />
            <Route path='new' element={<AttendeeForm />}/>
          </Route>
          <Route path='presentations'>
            <Route path='new' element={<PresentationForm />} />
          </Route>
          <Route index element={<MainPage />} />   
        </Routes>
      {/* </div> */}
    </BrowserRouter>
  );
}

export default App;
